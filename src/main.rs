#![no_main]
#![no_std]

use panic_semihosting as _;
use rtfm::app;

use cortex_m::asm;

// use heapless::mpmc::Q8;

use embedded_hal::digital::v2::{InputPin, OutputPin};
use stm32f1xx_hal::{
    prelude::*,
    pac,
    delay::Delay,
    usb,
    gpio::{gpioa, Output, PushPull, Analog, PullUp, Input, Pxx, OpenDrain, PullDown},
    adc::{SampleTime, Adc},
    timer::{Timer, CountDownTimer}
};
use usb_device::prelude::*;
use usb_device::bus::UsbBusAllocator;
use usb_device::class::UsbClass;
use usbd_serial::{SerialPort, USB_CLASS_CDC};
use ufmt::uwriteln;

mod hid;
mod joystick;
mod button_scanner;
mod averaging;

#[cfg(feature="joystick")]
const USB_NAME: &str = "joystick";
#[cfg(feature="throttle")]
const USB_NAME: &str = "throttle";

#[cfg(feature="joystick")]
const INVERT_AXES: bool = true;
#[cfg(feature="throttle")]
const INVERT_AXES: bool = false;


#[cfg(feature="joystick")]
define_scanner!(Scanner, 4, 6);
#[cfg(feature="throttle")]
define_scanner!(Scanner, 3, 1);

#[app(device = stm32f1xx_hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        usb_bus: &'static UsbBusAllocator<usb::UsbBusType>,
        usb_serial: usbd_serial::SerialPort<'static, usb::UsbBusType>,
        usb_joystick: hid::HidClass<'static, usb::UsbBusType, joystick::Joystick>,
        usb_device: UsbDevice<'static, usb::UsbBusType>,
        joy_pins: (gpioa::PA0<Analog>, gpioa::PA1<Analog>, gpioa::PA2<Analog>, gpioa::PA3<Analog>),
        adc: Adc<pac::ADC1>,
        timer: CountDownTimer<pac::TIM2>,
        scanner: Scanner<Pxx<Input<PullDown>>, Pxx<Output<PushPull>>>,
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        static mut USB_BUS: Option<UsbBusAllocator<usb::UsbBusType>> = None;

        // Alias peripherals
        let cp: cortex_m::Peripherals = ctx.core;
        let dp: pac::Peripherals = ctx.device;

        // Set up core registers
        let mut flash = dp.FLASH.constrain();
        let mut rcc = dp.RCC.constrain();

        let clocks = rcc.cfgr
            .use_hse(8.mhz())
            .sysclk(48.mhz())
            .pclk1(24.mhz())
            .adcclk(12.mhz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        // Set up GPIO registers
        let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);

        // Set up the USB port
        let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        usb_dp.set_low().unwrap();
        asm::delay(clocks.sysclk().0 / 100);

        let usb = usb::Peripheral {
            usb: dp.USB,
            pin_dm: gpioa.pa11,
            pin_dp: usb_dp.into_floating_input(&mut gpioa.crh),
        };

        let (usb_serial, usb_joystick, usb_device) = {
            *USB_BUS = Some(usb::UsbBus::new(usb));
            let serial = SerialPort::new(USB_BUS.as_ref().unwrap());

            let joystick = hid::HidClass::new(joystick::Joystick::new(), USB_BUS.as_ref().unwrap());

            let usb_dev = UsbDeviceBuilder::new(
                        USB_BUS.as_ref().unwrap(),
                        UsbVidPid(0x16c0, 0x27dd)
                    )
                .manufacturer("ZoqsHOTAS")
                .product(USB_NAME)
                .serial_number("TEST")
                .device_class(USB_CLASS_CDC)
                .build();

            (serial, joystick, usb_dev)
        };

        // Configure delay
        let mut delay = Delay::new(cp.SYST, clocks);

        // Set up ADC
        let mut adc = Adc::adc1(dp.ADC1, &mut rcc.apb2, clocks);
        adc.set_sample_time(SampleTime::T_239);
        let joy_pins = (
            gpioa.pa0.into_analog(&mut gpioa.crl),
            gpioa.pa1.into_analog(&mut gpioa.crl),
            gpioa.pa2.into_analog(&mut gpioa.crl),
            gpioa.pa3.into_analog(&mut gpioa.crl),
        );

        // Set up the timer
        let timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1)
            .start_count_down(2.hz());


        // Set up button scanner
        #[cfg(feature="joystick")]
        let scanner = Scanner::new(
                [
                    gpiob.pb12.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb13.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb14.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb15.into_pull_down_input(&mut gpiob.crh).downgrade(),
                ],
                [
                    gpiob.pb11.into_push_pull_output(&mut gpiob.crh).downgrade(),
                    gpiob.pb10.into_push_pull_output(&mut gpiob.crh).downgrade(),
                    gpiob.pb1.into_push_pull_output(&mut gpiob.crl).downgrade(),
                    gpiob.pb0.into_push_pull_output(&mut gpiob.crl).downgrade(),
                    gpioa.pa7. into_push_pull_output(&mut gpioa.crl).downgrade(),
                    gpioa.pa6. into_push_pull_output(&mut gpioa.crl).downgrade(),
                ]
            );
        // Set up button scanner
        #[cfg(feature="throttle")]
        let scanner = Scanner::new(
                [
                    gpiob.pb13.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb12.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb15.into_pull_down_input(&mut gpiob.crh).downgrade(),
                ],
                [
                    gpiob.pb14.into_push_pull_output(&mut gpiob.crh).downgrade(),
                ]
            );

        init::LateResources {
            usb_bus: USB_BUS.as_ref().unwrap(),
            usb_serial,
            usb_joystick,
            usb_device,
            adc,
            joy_pins,
            timer,
            scanner
        }
    }

    #[idle(resources = [usb_serial, usb_joystick, adc, joy_pins, timer, scanner])]
    fn idle(ctx: idle::Context) -> ! {
        let mut r = ctx.resources;
        let mut last_raw: i32 = 0;

        let mut x_average = averaging::Averaging::new();
        let mut y_average = averaging::Averaging::new();
        let mut z_average = averaging::Averaging::new();
        let mut rx_average = averaging::Averaging::new();
        loop {
            #[cfg(feature="throttle")]
            let x_bounds = (548., 1990., 3499.);
            #[cfg(feature="throttle")]
            let y_bounds = (359., 1823., 3260.);
            #[cfg(feature="joystick")]
            let x_bounds = (599., 2011., 3543.);
            #[cfg(feature="joystick")]
            let y_bounds = (533., 2002., 3423.);

            let z_bounds = (1142., 1866., 2670.);
            let rx_bounds = (0., 2048., 4096.);

            // Read joystick state
            let y_raw: u16 = r.adc.read(&mut r.joy_pins.0).expect("Adc read failed");
            let x_raw: u16 = r.adc.read(&mut r.joy_pins.1).expect("Adc read failed");
            let z_raw: u16 = r.adc.read(&mut r.joy_pins.2).expect("Adc read failed");
            let rx_raw: u16 = r.adc.read(&mut r.joy_pins.3).expect("Adc read failed");

            x_average.add_sample(x_raw);
            y_average.add_sample(y_raw);
            z_average.add_sample(z_raw);
            rx_average.add_sample(rx_raw);

            let x_value = x_average.current();
            let y_value = y_average.current();
            let z_value = z_average.current();
            let rx_value = rx_average.current();

            let z_raw = 0;
            let x = joy_value(r.adc, x_value, x_bounds);
            let y = joy_value(r.adc, y_value, y_bounds);
            #[cfg(feature="throttle")]
            let z = joy_value(r.adc, z_value, z_bounds);
            #[cfg(feature="joystick")]
            let z = 0;
            #[cfg(feature="throttle")]
            let rx = joy_value(r.adc, rx_value, rx_bounds);
            #[cfg(feature="joystick")]
            let rx = 0;

            if let Ok(()) = r.timer.wait() {
                let diff = last_raw - x_raw as i32;
                last_raw = x_raw as i32;
                r.usb_serial.lock(|serial| {
                    uwriteln!(
                        DirtyWriter(serial),
                        "x: {}, y: {}, z: {}, rx: {} ({}, {}, {}, {})\r",
                        x_value,
                        y_value,
                        z_value,
                        rx_value,
                        x,
                        y,
                        z,
                        rx,
                    ).unwrap();
                });
            }

            let mut button_buffer = [0; 3];
            r.scanner.scan_to_bytes(&mut button_buffer, 0);


            let report = joystick::JoystickReport::new(x, y, z, rx, button_buffer);
            r.usb_joystick.lock(|joy| {
                joy.device_mut().set_report(report.clone());
            });
            while let Ok(0) = r.usb_joystick.lock(|joy| {
                joy.write(&report.as_bytes())
            })
                {}
        }
    }


    #[task(binds = USB_LP_CAN_RX0, resources = [usb_device, usb_joystick, usb_serial])]
    fn usb_lp_can_rx0(cx: usb_lp_can_rx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial, cx.resources.usb_joystick);
    }

    #[task(binds = USB_HP_CAN_TX, resources = [usb_device, usb_joystick, usb_serial])]
    fn usb_hp_can_tx0(cx: usb_hp_can_tx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial, cx.resources.usb_joystick);
    }
};

fn usb_poll<B: usb_device::bus::UsbBus>(
    usb_dev: &mut UsbDevice<'static, B>,
    serial: &mut SerialPort<'static, B>,
    joystick: &mut hid::HidClass<'static, B, joystick::Joystick>,
) {
    if !usb_dev.poll(&mut [serial, joystick]) {
        return;
    }
    joystick.poll();
    let mut buf = [0;10];
    match serial.read(&mut buf) {
        Ok(_) => {},
        Err(UsbError::WouldBlock) => {},
        e => panic!("USB read error: {:?}", e)
    }
}


fn joy_value(
    adc: &Adc<pac::ADC1>,
    value: u16,
    (min, center, max): (f32, f32, f32)
) -> i8 {
    let inversion = if INVERT_AXES {-1} else {1};
    let range = max - min;

    let frac = ((value as f32) - center) / (range);

    let bounded = if frac < -0.5 {-0.5} else {if frac > 0.5 {0.5} else {frac}};
    inversion * (255. * (bounded)) as i8
}




pub struct DirtyWriter<'a, B: 'static + usb_device::bus::UsbBus>(
    &'a mut SerialPort<'static, B>
);

impl<'a, B: usb_device::bus::UsbBus> ufmt::uWrite for DirtyWriter<'a, B> {
    type Error = usb_device::UsbError;
    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        match self.0.write(&s.as_bytes()) {
            Ok(_) => Ok(()),
            Err(UsbError::WouldBlock) => Ok(()),
            Err(e) => Err(e)
        }
    }
}

